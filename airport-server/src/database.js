const Sequelize = require("sequelize");
const dbConfig = require("./config/db.config");

/**
 * Database initialization.
 */
const sequelize = new Sequelize({
    dialect: dbConfig.dialect,
    storage: dbConfig.storage,
    pool: dbConfig.pool,
    logging: true
});

module.exports = sequelize;