/**
 * index.js is the entry point of the airport-server.
 * This layer is the one loader by Node.js.
 */

const express = require("express"),
    winston = require("winston"),
    swaggerUi = require('swagger-ui-express'),
    swaggerFile = require('../swagger-output.json'),
    db = require("./database");

/**
 * Definition of a logger.
 */
const logger = winston.createLogger({
    level: "info",
    format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.json()
    ),
    transports: [
        new winston.transports.Console()
        // file logger
        //new winston.transports.File({ filename: "logs/app.log" }),
    ]
});

/**
 * Database initialization and connectivity.
 */
db.authenticate()
    .then(() => {
        (async () => {
            initWebServer()

            await db.sync(db.syncMode);
            logger.info('Database connection has been established and the database is synchronized.');
        })();
    })
    .catch(err => {
        logger.error('Unable to connect to the database {}', err);
    });

function initWebServer() {
    const serverConfig = require("./config/server.config");
    const cors = require('cors')

    const app = express()
    app.use(cors())
    app.use(express.json())
    app.use(serverConfig.swaggerUiEndpoint, swaggerUi.serve, swaggerUi.setup(swaggerFile))
    app.listen(serverConfig.port, () => {
        logger.info(`Server is running on port ${serverConfig.port}`)
    })
    manageRoutes(app);
}

function manageRoutes(app) {
    app.get('/', (req, res) => {
        res.send("Airport management")
    })

    const passengerRoutes = require('./passenger/passenger-routes.js');
    app.use("/passengers", passengerRoutes);
}
