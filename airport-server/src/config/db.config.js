/**
 * Database config.
 */

module.exports = {
    syncMode: {alter: true},
    storage: process.env.HOME + '/airport.db',
    dialect: "sqlite",
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
};