/**
 * Web server config.
 */
module.exports = {
    port: 8080,
    swaggerUiEndpoint: '/swagger-ui'
};