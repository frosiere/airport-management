/**
 * Repository config.
 */

RepositoryType = {
    SEQUELIZE: "sequelize",
    IN_MEMORY: "in-memory"
}

module.exports = {
    RepositoryType,
    repositoryType: RepositoryType.SEQUELIZE
};