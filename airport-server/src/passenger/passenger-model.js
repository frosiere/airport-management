/**
 * Defines the passenger model. The model is only used into
 * the in-memory-passenger-repository.
 */
class Passenger {
    constructor(id, firstName, lastName, birthDate) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
    }
}

module.exports = {Passenger};