const express = require('express'),
    {PassengerService} = require("./passenger-service");

const passengerService = createPassengerService();

const router = express.Router();

/**
 * Defines the routes related to the passenger.
 */
router.get('/', (req, res) => {
    const allPassengers = passengerService.getAllPassengers();
    allPassengers.then((result) => {
        return res.json(result);
    }).catch((error) => {
        throw new Error(error);
    });
})

router.get('/:id', (req, res) => {
    const id = parseInt(req.params.id)
    passengerService.getPassenger(id).then((result) => {
        return res.json(result);
    }).catch((error) => {
        throw new Error(error);
    });
})

router.post('/', (req, res) => {
    const newPassenger = req.body;
    passengerService.createPassenger(newPassenger.id, newPassenger.firstName, newPassenger.lastName, newPassenger.birthDate).then((result) => {
        return res.json(result);
    }).catch((error) => {
        throw new Error(error);
    })
})

router.delete('/:id', (req, res) => {
    const id = parseInt(req.params.id)
    passengerService.deletePassenger(id).then((result) => {
        return res.json(result);
    }).catch((error) => {
        throw new Error(error);
    });
})

module.exports = router;

function createPassengerService() {
    const repositoryConfig = require("../config/repository.config");
    const {InMemoryPassengerRepository} = require("./repository/in-memory-passenger-repository");
    const {SequelizePassengerRepository} = require("./repository/sequelize-passenger-repository");
    return new PassengerService(
        repositoryConfig.RepositoryType.IN_MEMORY === repositoryConfig.repositoryType
            ? new InMemoryPassengerRepository()
            : new SequelizePassengerRepository()
    );
}