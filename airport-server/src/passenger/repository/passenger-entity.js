const sequelize = require("../../database");
const {DataTypes, Model} = require("sequelize");

/**
 * Defines a passenger entity. The entity makes the link between the code
 * and the database.
 */
class PassengerEntity extends Model {
}

PassengerEntity.init(
    {
        id: {
            type: DataTypes.INTEGER,
                autoIncrement: true,
                primaryKey: true,
        },

        firstName: {
            type: DataTypes.STRING,
                allowNull: false,
        },

        lastName: {
            type: DataTypes.STRING,
                allowNull: false,
        },

        birthDate: {
            type: DataTypes.DATE,
                allowNull: false,
        }
    },
    {
        sequelize,
        modelName: 'Passenger',
        indexes: [{unique: true, fields: ["id"]}],
            underscored: true,
        version: true // enable the optimistic locking
    }
)

/*const PassengerEntity = sequelize.define(
    "Passenger",
    {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true,
        },

        firstName: {
            type: DataTypes.STRING,
            allowNull: false,
        },

        lastName: {
            type: DataTypes.STRING,
            allowNull: false,
        },

        birthDate: {
            type: DataTypes.DATE,
            allowNull: false,
        }
    },
    {
        indexes: [{unique: true, fields: ["id"]}],
        underscored: true,
        version: true // enable the optimistic locking
    }
);*/

module.exports = {
    PassengerEntity,
    sequelize
};