const {PassengerEntity} = require("./passenger-entity");

/**
 * Sequelize implementation of the passenger repository.
 *
 * This implementations uses the Sequelize library to manage
 * the {@link PassengerEntity} lifecycle (CRUD operations).
 */
class SequelizePassengerRepository {

    findOne(id) {
        return PassengerEntity.findByPk(id);
    }

    findAll() {
        return PassengerEntity.findAll();
    }

    createPassenger(id, firstName, lastName, birthDate) {
        return PassengerEntity.create({
            firstName: firstName,
            lastName: lastName,
            birthDate: birthDate
        });
    }

    deletePassenger(id) {
        return PassengerEntity.destroy(
            {where: {id: id}}
        );
    }
}

module.exports = {SequelizePassengerRepository};