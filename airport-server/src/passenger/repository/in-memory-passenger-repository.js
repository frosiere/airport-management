const {Passenger} = require("../passenger-model");

/**
 * In-memory implementation of the passenger repository.
 *
 * Passengers are stored in a {@code Map}.
 */
class InMemoryPassengerRepository {

    #passengers = new Map();

    findOne(id) {
        const existingPassenger = this.#passengers.get(id);
        if (existingPassenger == null) {
            throw new Error("No passenger can be found for the given identifier " + id)
        }
        return Promise.resolve(existingPassenger);
    }

    findAll() {
        return Promise.resolve(Array.from(this.#passengers.values()));
    }

    createPassenger(id, firstName, lastName, birthDate) {
        const newPassenger = new Passenger(id, firstName, lastName, birthDate);
        this.#passengers.set(id, newPassenger)
        return Promise.resolve(newPassenger);
    }

    deletePassenger(id) {
        const existingPassenger = this.#passengers.get(id);
        if (existingPassenger == null) {
            throw new Error("No passenger can be found for the given identifier " + id)
        }
        return Promise.resolve(this.#passengers.delete(id));
    }
}

module.exports = {InMemoryPassengerRepository};