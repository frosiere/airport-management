/**
 * Service for passenger. The service is used to manage the business logic.
 * The link with the database is delegated to the repository.
 */
class PassengerService {

    constructor(passengerRepository) {
        this.passengerRepository = passengerRepository;
    }

    getPassenger(id) {
        return this.passengerRepository.findOne(id);
    }

    getAllPassengers() {
        return this.passengerRepository.findAll()
    }

    createPassenger(id, firstName, lastName, birthDate) {
        return this.passengerRepository.createPassenger(id, firstName, lastName, birthDate)
    }

    deletePassenger(id) {
        return this.passengerRepository.deletePassenger(id)
    }
}

module.exports = {PassengerService};